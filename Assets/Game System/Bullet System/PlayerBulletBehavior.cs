﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameSystem
{
    namespace BulletSystem
    {
        public class PlayerBulletBehavior : MonoBehaviour
        {
            [SerializeField] Vector3 bulletXSpeed;    // bullet speed in x direction
            [SerializeField] float bulletLiftime = 4f;
            private float bulletDirection;
            
            void Start()
            {
                // find player x direction and set bullet x direction the same as player
                bulletDirection = FindObjectOfType<PlayerMovementSystem.PlayerMovement>().FindDirection();
                Destroy(gameObject, bulletLiftime);
            }

            void Update()
            {
                Move();
            }

            private void Move()
            {
                transform.position += bulletXSpeed * bulletDirection * Time.deltaTime;
            } 

            public void OnCollisionEnter2D(Collision2D collision) 
            {
                if(collision.gameObject.tag == "Enemy")
                {
                Destroy(collision.gameObject);
                Destroy(gameObject);
                }
            }
        }
    }
}


