﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GameSystem
{
    namespace EnemyBehavoirSystem
    {
        public class Enemy1Movement : MonoBehaviour
        {
            [SerializeField] Vector3 enemyMovement;
            
            void Start()
            {
                Destroy(gameObject, 20f);
            }
            
            void Update()
            {
                Move();
            }

            private void Move()
            {
                transform.position  += enemyMovement;
            }
        }
    }
}


