﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GameSystem
{
    namespace EnemyBehavoirSystem
    {
        public class Enemy2Movement : MonoBehaviour
        {
            [SerializeField] Vector3 enemyMovement;
            [SerializeField] float waveSize;
            [SerializeField] float waveNum;
            
            void Start()
            {
                Destroy(gameObject, 20f);
            }
            
            void Update()
            {
                Move();
            }

            private void Move()
            {
                transform.position  += enemyMovement;
                // set enemy2 to move in sin wave shape toward the player
                float yPos = Mathf.Sin(transform.position.x * waveNum) * waveSize;
                Vector2 newPosition = new Vector2(transform.position.x, yPos);
                transform.position = newPosition;
            }
        }
    }
}


