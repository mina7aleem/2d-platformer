using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GameSystem
{
    namespace Enemy2BulletSystem
    {
        
        public class Enem2BulletBehavoir : MonoBehaviour
        {
            [SerializeField] Vector3 bulletXSpeed; // bullet speed in x direction
            
            void Update()
            {
                Move();
            }

            private void Move()
            {
                transform.position += bulletXSpeed * Time.deltaTime;
            } 
        }
    }
}


