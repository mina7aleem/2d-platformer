﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GameSystem
{
    namespace FinishLevelSystem
    {
        public class FinishLevel : MonoBehaviour
        {
            private void OnTriggerEnter2D(Collider2D other) 
            {
                if(other.tag == "Player")
                {
                    Debug.Log("Congratulations you have passed level 1");
                    // load congrats. you won scene
                    SceneManager.LoadScene(2);
                }
            }
        }
    }
}


