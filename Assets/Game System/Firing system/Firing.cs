﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameSystem
{
    namespace FiringSystem
    {
        public class Firing : MonoBehaviour
        {
            [SerializeField] InstantiationSystem.InstantiateObject instObject; // reference to instantiate object script
            [SerializeField] GameObject fireBall;
            [SerializeField] Transform instPoint; // player bullet instantiation point
            
            void Update()
            {
                Fire();
            }

            private void Fire()
            {
                // when left mouse button clicked, player fire the instantiated bullet
                if(Input.GetMouseButtonDown(0) == true)
                {
                    instObject.InstantiateGameobject(fireBall, instPoint);
                }
            }
        }
    }
}


