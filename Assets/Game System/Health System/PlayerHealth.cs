﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace GameSystem
{
    namespace HealthSystem
    {
        public class PlayerHealth : MonoBehaviour
        {
            [SerializeField] Image healthBarImage;
            private bool reduceHealth;
            
            void Update()
            {
                PlayerTakeDamage();
            }
            // when player touches obstacle, his health decreases and the hearts no. decreases till he gets game over
            public void PlayerTakeDamage()
            {
                if(reduceHealth && healthBarImage.fillAmount > 0.25f)
                {
                    reduceHealth = false;
                    healthBarImage.fillAmount = healthBarImage.fillAmount - (1/3f);
                }
                else if(reduceHealth && healthBarImage.fillAmount == 0)
                {
                    Debug.LogWarning("Game Over");
                    // load game over scene
                    SceneManager.LoadScene(0);
                    reduceHealth = false;
                }
            }

            private void OnCollisionEnter2D(Collision2D collision)
            {
                if(collision.gameObject.tag == "Obstacle")
                {
                    reduceHealth = true;
                }
            }
            // checks if player fall from the ground and touches the "underground" collider 
            private void OnTriggerEnter2D(Collider2D other) 
            {
                if(other.tag == "UnderGround")
                {
                    Debug.LogWarning("Game Over");
                    // load the game over scene
                    SceneManager.LoadScene(0);
                }
            }
        }
    }
}


