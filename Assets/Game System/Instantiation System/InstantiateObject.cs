﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameSystem
{
    namespace InstantiationSystem
    {
        public class InstantiateObject : MonoBehaviour
        {
            public void InstantiateGameobject(GameObject objectToInstantiate, Transform instPoint)
            {
                Instantiate(objectToInstantiate, instPoint.position, Quaternion.identity);
            }
        }
    }
}


