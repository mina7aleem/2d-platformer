﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameSystem
{
    namespace PickupsSystem
    {
        public class ChangeColorPickups : MonoBehaviour
        {
            private Color32 blueColor = new Color32(64, 81, 245, 255);
            private Color32 normalColor = new Color32(255, 255, 255, 255);
            private SpriteRenderer playerSpriteRenderer;

            void Start()
            {
                playerSpriteRenderer = GetComponent<SpriteRenderer>();
            }
            // if player touches the blue potion, his color changes to blue. if he touches the red, his color back to normal red
            private void OnTriggerEnter2D(Collider2D other) 
            {
                if(other.tag == "BlueColor")
                {
                    playerSpriteRenderer.color = blueColor;
                }
                else if(other.tag == "NormalColor")
                {
                    playerSpriteRenderer.color = normalColor;
                }
            }
        }
    }
}

