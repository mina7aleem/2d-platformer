 using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GameSystem
{
    namespace PlayerMovementSystem
    {
        public class PlayerMovement : MonoBehaviour
        {
            private Rigidbody2D rb2d;
            [SerializeField] float moveSpeed;
            [SerializeField] float jumpSpeed;
            private Animator animator;
            private bool canJump;

            void Start()
            {
                rb2d = GetComponent<Rigidbody2D>();
                animator = GetComponent<Animator>();
            }

            void Update()
            {
                Move(); 
                Jump();
            }

            private void Move()
            {
                // move player on x axis
                float horizontalInput = Input.GetAxis("Horizontal");
                rb2d.velocity = new Vector2(horizontalInput * moveSpeed/*  * Time.deltaTime */, rb2d.velocity.y);
               // flip face, animation (idle and walk)
                if(horizontalInput > Mathf.Epsilon)
                {
                    transform.localScale = new Vector3(1, 1, 1);
                    animator.SetBool("isWalking", true);
                }
                else if(horizontalInput < -Mathf.Epsilon)
                {
                    transform.localScale = new Vector3(-1, 1, 1);
                    animator.SetBool("isWalking", true);
                }
                else
                {
                    animator.SetBool("isWalking", false);
                }            
            }
            
            // jump, animation jump
            private void Jump()
            {
                if(Input.GetKey(KeyCode.Space) && canJump)
                {
                    rb2d.velocity = new Vector2(rb2d.velocity.x, jumpSpeed/*  * Time.deltaTime */);
                    canJump = false;
                    animator.SetBool("isJumping", true);
                }
                else
                {
                    animator.SetBool("isJumping", false);
                }
            }
            // jump only if on ground
            private void OnCollisionEnter2D(Collision2D collision)
            {
                if(collision.gameObject.tag == "Ground")
                {
                    canJump = true;
                }

                if(collision.gameObject.tag == "Enemy") 
                {
                    Destroy(collision.gameObject);
                    Destroy(gameObject);
                    Debug.LogWarning("Game Over");
                    // load game over scene
                    SceneManager.LoadScene(0);
                }
            }

            // find player direction to set bullet direction
            public float FindDirection()
            {
                return Mathf.Sign(transform.localScale.x);
            }
        }
    }
}

