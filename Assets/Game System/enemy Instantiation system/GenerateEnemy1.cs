﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameSystem
{
    namespace enemyInstantiationsystem
    {
        public class GenerateEnemy1 : MonoBehaviour
        {
            public InstantiationSystem.InstantiateObject instEnemy1;
            [SerializeField] GameObject enemy1;
            void Start()
            {
                InvokeRepeating("InstEnemyFunc", 4f, 3f);
                Destroy(gameObject, 20f);
            }

            private void InstEnemyFunc()
            {
                // instantiate enemy at certain distance from player
                GameObject instPoint = GameObject.FindWithTag("Enemy1InstPoint");
                Transform enemy1InstPoint = instPoint.transform;
                instEnemy1.InstantiateGameobject(enemy1, enemy1InstPoint);
            }
        }
    }
}


