﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameSystem
{
    namespace enemyInstantiationsystem
    {
        public class GenerateEnemy2 : MonoBehaviour
        {
            public InstantiationSystem.InstantiateObject instEnemy2;
            [SerializeField] GameObject enemy2;
            //[SerializeField] Transform enemy2InstPoint;
            void Start()
            {
                InvokeRepeating("InstEnemyFunc", 4f, 3f);
                Destroy(gameObject, 20f);
            }

            private void InstEnemyFunc()
            {
                // instantiate enemy at certain distance from player
                GameObject instPoint = GameObject.FindWithTag("Enemy2InstPoint");
                Transform enemy2InstPoint = instPoint.transform;
                instEnemy2.InstantiateGameobject(enemy2, enemy2InstPoint);
            }
        }
    }
}
